FROM maven:3-jdk-8
COPY target/*.jar /
EXPOSE 8080
ENTRYPOINT ["java","-jar","/java-maven-junit-helloworld-1.0-SNAPSHOT.jar"]
